let ingradients = {
    1: {
        id: 1,
        title: "in 1",
    },
    2: {
        id: 2,
        title: "in 2",
    },
    3: {
        id: 3,
        title: "in 3",
    },
    4: {
        id: 4,
        title: "in 4",
    },
};

let dishes = [
    {
        title: "ttt",
        ingradients: [
            {
                id: 1,
                gram: 100,
            }
        ],
    }
];

function renderIngradients(){
    let ingradientsElement = document.getElementById("ingradients");
    for(let i in ingradients){
        let inputCheckbox = document.createElement("INPUT");
        let inputGram = document.createElement("INPUT");
        let label = document.createElement("LABEL");
        let div = document.createElement("DIV");

        inputCheckbox.type = "checkbox";
        inputCheckbox.id = "in" + ingradients[i].id;
        inputCheckbox.name = "in";
        inputCheckbox.value = ingradients[i].id;

        label.setAttribute("for", "in" + ingradients[i].id);
        label.innerHTML = ingradients[i].title;

        inputGram.type = "text";
        inputGram.value = "";
        inputGram.name = "in" + ingradients[i].id;

        div.appendChild(inputCheckbox);
        div.appendChild(label);
        div.appendChild(inputGram);
        ingradientsElement.appendChild(div);
    }
};

function renderDishes(){
    let dishesElement = document.getElementById("dishes");
    dishesElement.innerHTML = "";
    for(let i in dishes){
        let li = document.createElement("LI");
        li.innerHTML = dishes[i].title;
        dishesElement.appendChild(li);
    }
}

renderIngradients();
renderDishes();

let submit = document.querySelector(".submit");

submit.addEventListener("click", event => {
    let form = document.getElementById("form");
    let formData = new FormData(form);

    let ingrds = formData.getAll("in");
    let title = formData.get("title_dish");

    let valueIngradients = [];

    for(let i = 0; i < ingrds.length; i++){
        valueIngradients.push({
            id: +ingrds[i],
            gram: +formData.get("in" + ingrds[i]),
        });
    }

    let dish = {
        title,
        ingradients: valueIngradients,
    };

    dishes.push(dish);

    renderDishes();
});

document.getElementById("show").addEventListener("click", event => {console.log(dishes)});

/*function newStudents(){
    var body = document.querySelector('body');
    console.log(body)
    var form = body.createElement('form');
    form.setAttribute('id', 'form');
    form.setAttribute('action', '/send/');
    form.setAttribute('method', 'POST');
    form.setAttribute('onsubmit', 'return false;');
    body.appendChild(form);





}
newStudents();*/