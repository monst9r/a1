window.onload = function(){
    let students = [
        {
            firstname: "Негр Иван, нажми, что б убить его",
            course: 2,
            avg_ball: 4.4,
        },
        {
            firstname: "Негр Джон, нажми, что б убить его",
            course: 2,
            avg_ball: 3.4,
        },
        {
            firstname: "Негр Фил, нажми, что б убить его",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Негр Бруно, нажми, что б убить его",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Негр Петро, нажми, что б убить его",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Негр Майкл, нажми, что б убить его",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Негр Сью, нажми, что б убить его",
            course: 3,
            avg_ball: 4.3,
        },
    ];

    function renderStudents(){
        var ul = document.createElement("UL");
        for(let i = 0; i < students.length; i++){
            var li = document.createElement("LI");
            li.innerHTML = students[i].firstname;

            (function (i){
                li.addEventListener("click", function(event){
                    students.splice(i, 1);
                    renderStudents();
                });
            })(i);

            ul.appendChild(li);
        }
        let list = document.querySelector(".list");
        list.innerHTML = "";
        list.appendChild(ul);
    }

    document.querySelector(".create").addEventListener("click", function(event){
        renderStudents();
    });
};

/*
масив
10

40b

++a; - увеличевает на единицу - записует
--a; - уменшает на еденицу - записует
a++; - записует - увеличевает на единицу
a--; - записует - уменшает на еденицу
*/ 

/*
+=
-=
*=
/=
var a = 2;
var b = 1;

b -= a; b = b - a;

цыклы
for(initialization;conditions;step){

}
while(condition; ) {

}

do(condition){

}

*/