'use strict'
/*
В задании из пятого урока, взять массив со студентами и вывести их на страницу согласно сверстанной HTML-структуре, рядом с каждым студентом 
вывести крестик - по нажатию на который студент будет удален (удаляется как со страницы, так и с объекта), если был удален последний студент 
написать текстовое сообщение (“Студенты не найдены”)
*/
window.onload = function(){ 
    var students = [
    {name: 'Ivan', 
    estimate: 4, 
    course: 1, 
    active: true
},
    {name: 'Petro', 
        estimate: 3, 
        course: 1, 
        active: true
    },
    {name: 'Sergiy', 
        estimate: 2, 
        course: 4, 
        active: false
    },
    {name: 'Vasilii',
        estimate: 5, 
        course: 2, 
        active: true
    },
    {name: 'Vitalii',
        estimate: 5, 
        course: 2, 
        active: true
    },
    {name: 'Jurii',
        estimate: 3, 
        course: 3, 
        active: true
    },
    {name: 'Artem',
        estimate: 2, 
        course: 4, 
        active: true
    },
    {name: 'Evgenii',
        estimate: 5, 
        course: 1, 
        active: true
    },
    {name: 'Volodya',
        estimate: 4, 
        course: 3, 
        active: false
    },
    {name: 'Sasha',
        estimate: 4, 
        course: 4, 
        active: true
    },
    {name: 'Silvestr',
        estimate: 3, 
        course: 2, 
        active: true
    },
    {name: 'Mikle',
        estimate: 2, 
        course: 1, 
        active: false
    }
]


function averageEstimatePerCourse(){
    let masArr = [];
    let courseArr = [];
    let estimateArr = [];
    let est;
    let resultArr = {};

    for(let i = 0; i < students.length; i++){
       if(students[i].active == true){
           masArr.push(students[i]); 
        }   
    }
    for(let i in masArr){
    courseArr.push(masArr[i].course)
    }     
     let courseUnique = [];
    for(let i in courseArr){
        if(courseUnique.indexOf(courseArr[i]) == -1){
            courseUnique.push(courseArr[i])
        }
    }
    for(let i in courseUnique){
        resultArr[courseUnique[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(resultArr).length; i++){
    let estimateAverage = [];
    for(let j in masArr){
        if(masArr[j].course == i){
            estimateAverage.push(masArr[j].estimate)
            resultArr[i] += masArr[j].estimate
        }
    }
    resultArr[i] = resultArr[i]/estimateAverage.length
}
   console.log('Средняя оценка по курсам:')
   console.log(resultArr)
}
averageEstimatePerCourse();


function unActiveStudents(){
    let unActiveStud = [];
    
    let courseStudents = {};
    for(let i in students){
        if(students[i].active == false){
            unActiveStud.push(students[i]);
        }
    }
    console.log('Всего неактивных студентов: ' + Object.keys(unActiveStud).length)
   
    let courseUnique = [];
    for(let i in students){
        if(courseUnique.indexOf(students[i].course) == -1){
            courseUnique.push(students[i].course)
        }
    }
    for(let i in courseUnique){
        courseStudents[courseUnique[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(courseStudents).length; i++){
        let unActiveStudPerCourse = [];
    for(let j in students){
        if(students[j].course == i && students[j].active == false){
            unActiveStudPerCourse.push(true)     
        }
    }
    courseStudents[i] = unActiveStudPerCourse.length
}
console.log('Неактивных студентов по курсам:')
console.log(courseStudents)
}
unActiveStudents();

function createStud(){
    let list = document.querySelector('.list');
        for(let i in students){
            
            var createStudents = document.createElement('stud');
            createStudents.setAttribute('class', 'p');
            createStudents.innerHTML = students[i].name + ' x';
            (function (i){
                createStudents.addEventListener('click', function(event){
                    students.splice(i, 1);
                    list.innerHTML = '';
                    createStud();
                });
            })(i);
            list.appendChild(createStudents);
            
        }
        if(document.querySelector('stud') == null){
            let noStud = document.createElement('noStud');
            noStud.setAttribute('class', 'p');
            noStud.innerHTML = 'Студентов больше нет!';
            list.appendChild(noStud);
        }
    function avgEstimatePerCourse(){
        let courseArr = [];
        let courseUnique = [];
        let resultArr = {};
      
        for(let i in students){
            courseArr.push(students[i].course);
        }
        for(let i in courseArr){
            if(courseUnique.indexOf(courseArr[i]) == -1){
                courseUnique.push(courseArr[i]);
            }
        }
        for(let i in courseUnique){
            resultArr[courseUnique[i]] = 0;
        }
        for(let i  = 1; i <= Object.keys(resultArr).length; i++){
            let estimateAverage = [];
            for(let j in students){
                if(students[j].course == i){
                    estimateAverage.push(students[j].estimate);
                    resultArr[i] += students[j].estimate;
                }
            }
            resultArr[i] = Math.round(resultArr[i]/estimateAverage.length);
        }
        console.log(resultArr);

        for(let i in resultArr){
        var averageOut = document.createElement('estimate');
        averageOut.setAttribute('class', 'p');
            averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
            list.appendChild(averageOut);
        }
    }avgEstimatePerCourse();

    function unActive(){
        let unActiveStud = [];
        var unActiveStudents = document.createElement('unActive');
        unActiveStudents.setAttribute('class', 'p');

        for(let i in students){
            if(students[i].active == false){
                unActiveStud.push(students[i]);
            }
        }
        unActiveStudents.innerHTML = 'Всего неактивных студентов: ' + Object.keys(unActiveStud).length;
        list.appendChild(unActiveStudents);
    }unActive();
}createStud();

function createNewStudents(){
    let submit = document.querySelector('.submit');
 
    submit.addEventListener('click', event => {
        let form = document.querySelector('form');
        let chbox = document.getElementById('chbox');
        let isActive;
        if(chbox.checked){
            isActive = true;
        }else{
            isActive = false;
        }
        for(let i = 0; i < 3; i++){
        let newStudent = {name: form[i].value,
                         estimate: parseInt(form[i+1].value),
                         course: parseInt(form[i+2].value),
                         active: isActive 
                     }
                     students.push(newStudent);
                     console.log(newStudent);
                     console.log(students);
                     console.log(isActive)
                     break;
                 }
                 document.querySelector('.list').innerHTML = '';
                 createStud();
    })
 }createNewStudents();


}
