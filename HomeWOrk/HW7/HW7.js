'use strict'

window.onload = function(){ 
    var students = [
    {name: 'Ivan', 
    estimate: 4, 
    course: 1, 
    active: true
},
    {name: 'Petro', 
        estimate: 3, 
        course: 1, 
        active: true
    },
    {name: 'Sergiy', 
        estimate: 2, 
        course: 4, 
        active: false
    },
    {name: 'Vasilii',
        estimate: 5, 
        course: 2, 
        active: true
    },
    {name: 'Vitalii',
        estimate: 5, 
        course: 2, 
        active: true
    },
    {name: 'Jurii',
        estimate: 3, 
        course: 3, 
        active: true
    },
    {name: 'Artem',
        estimate: 2, 
        course: 4, 
        active: true
    },
    {name: 'Evgenii',
        estimate: 5, 
        course: 1, 
        active: true
    },
    {name: 'Volodya',
        estimate: 4, 
        course: 3, 
        active: false
    },
    {name: 'Sasha',
        estimate: 4, 
        course: 4, 
        active: true
    },
    {name: 'Silvestr',
        estimate: 3, 
        course: 2, 
        active: true
    },
    {name: 'Mikle',
        estimate: 2, 
        course: 1, 
        active: false
    }
]


function averageEstimatePerCourse(){
    let masArr = [];
    let courseArr = [];
    let estimateArr = [];
    let est;
    let resultArr = {};

    for(let i = 0; i < students.length; i++){
       if(students[i].active == true){
           masArr.push(students[i]); 
        }   
    }
    for(let i in masArr){
    courseArr.push(masArr[i].course)
    }     
     let courseUnique = [];
    for(let i in courseArr){
        if(courseUnique.indexOf(courseArr[i]) == -1){
            courseUnique.push(courseArr[i])
        }
    }
    for(let i in courseUnique){
        resultArr[courseUnique[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(resultArr).length; i++){
    let estimateAverage = [];
    for(let j in masArr){
        if(masArr[j].course == i){
            estimateAverage.push(masArr[j].estimate)
            resultArr[i] += masArr[j].estimate
        }
    }
    resultArr[i] = resultArr[i]/estimateAverage.length
}
   console.log('Средняя оценка по курсам:')
   console.log(resultArr)
}
averageEstimatePerCourse();


function unActiveStudents(){
    let unActiveStud = [];
    
    let courseStudents = {};
    for(let i in students){
        if(students[i].active == false){
            unActiveStud.push(students[i]);
        }
    }
    console.log('Всего неактивных студентов: ' + Object.keys(unActiveStud).length)
   
    let courseUnique = [];
    for(let i in students){
        if(courseUnique.indexOf(students[i].course) == -1){
            courseUnique.push(students[i].course)
        }
    }
    for(let i in courseUnique){
        courseStudents[courseUnique[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(courseStudents).length; i++){
        let unActiveStudPerCourse = [];
    for(let j in students){
        if(students[j].course == i && students[j].active == false){
            unActiveStudPerCourse.push(true)     
        }
    }
    courseStudents[i] = unActiveStudPerCourse.length
}
console.log('Неактивных студентов по курсам:')
console.log(courseStudents)
}
unActiveStudents();

function createStud(){
    let list = document.querySelector('.list');
    let table = document.createElement('table');
    list.appendChild(table);
    let tr = document.createElement('tr');
    table.appendChild(tr);
    let th1 = document.createElement('th');
    let th2 = document.createElement('th');
    let th3 = document.createElement('th');
    let th4 = document.createElement('th');
    th1.innerHTML = 'First name';
    th2.innerHTML = 'Estimate';
    th3.innerHTML = 'Course';
    th4.innerHTML = 'Active';
    tr.appendChild(th1);
    tr.appendChild(th2);
    tr.appendChild(th3);
    tr.appendChild(th4);
    
        for(let i in students){
            var trTr = document.createElement('tr')
            table.appendChild(trTr);
            var createStudents = document.createElement('td');
            createStudents.id = i +'n';
            var changeActive = document.createElement('input');
            var deleteStud = document.createElement('img');
            var estimateT = document.createElement('td');
            estimateT.id = i + 'e';
            var courseT = document.createElement('td');
            courseT.id = i + 'c';
            estimateT.innerHTML = students[i].estimate;
            courseT.innerHTML = students[i].course;
            deleteStud.src = 'krest.png';
            deleteStud.width = '12';
            deleteStud.height = '12';
            changeActive.type = 'submit';
            if(students[i].active == true){
                changeActive.value = 'active';
            }else if(students[i].active == false){
                changeActive.value = 'un active';
            }
            changeActive.id = 'changeActiveStudents';
             
                if(students[i].estimate <= 3){
                    trTr.setAttribute('class', 'less');
                }else if(students[i].estimate >= 4 && students[i].estimate <5){
                    trTr.setAttribute('class', 'midle');
                }else if(students[i].estimate >= 5){
                    trTr.setAttribute('class', 'more');
                }
            createStudents.innerHTML = students[i].name;
            
            (function (i){
                createStudents.addEventListener('click', function(event){
                    let idName = i +'n';
                    let tdFirstName = document.getElementById(idName);
                    let inputName = document.createElement('input');
                    inputName.type = 'text';
                    inputName.className = 'input';
                    inputName.value = students[i].name;
                    tdFirstName.innerHTML = '';
                    tdFirstName.appendChild(inputName);
                    inputName.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            students[i].name = event.target.value;
                            list.innerHTML = '';
                            createStud();
                        }
                    });
                    inputName.focus();
                });
            })(i);

            (function (i){
                estimateT.addEventListener('click', function(event){
                    let idEstimate = i +'e';
                    let tdEstimate = document.getElementById(idEstimate);
                    let inputEstimate = document.createElement('input');
                    inputEstimate.type = 'text';
                    inputEstimate.className = 'input';
                    inputEstimate.value = students[i].estimate;
                    tdEstimate.innerHTML = '';
                    tdEstimate.appendChild(inputEstimate);
                    inputEstimate.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            students[i].estimate = parseInt(event.target.value);
                            list.innerHTML = '';
                            createStud();
                            console.log(students);
                        }
                    });
                    inputEstimate.focus();
                });
            })(i);

            (function (i){
                courseT.addEventListener('click', function(event){
                    let idCourse = i +'c';
                    let tdCourse = document.getElementById(idCourse);
                    let inputCourse = document.createElement('input');
                    inputCourse.type = 'text';
                    inputCourse.className = 'input';
                    inputCourse.value = students[i].course;
                    tdCourse.innerHTML = '';
                    tdCourse.appendChild(inputCourse);
                    inputCourse.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            students[i].course = parseInt(event.target.value);
                            list.innerHTML = '';
                            createStud();
                            console.log(students);
                        }
                    });
                    inputCourse.focus();
                });
            })(i);

            (function (i, changeActive){
                changeActive.addEventListener('click', function(event){
                    if(changeActive.value == 'active'){
                        students[i].active = false;
                        list.innerHTML = '';
                        createStud();
                    }else if(changeActive.value == 'un active'){
                        students[i].active = true;
                        list.innerHTML = '';
                        createStud();
                    }
                })
            })(i, changeActive);
            (function (i){
                deleteStud.addEventListener('click', function(event){
                    students.splice(i, 1);
                    list.innerHTML = '';
                    createStud();
                });
            })(i);
            
            trTr.appendChild(createStudents);
            trTr.appendChild(estimateT);
            trTr.appendChild(courseT);
            trTr.appendChild(changeActive);  
            trTr.appendChild(deleteStud);
            
            
           
            
        }

        if(document.querySelector('.less') == null && document.querySelector('.midle') == null && document.querySelector('.more') == null){
            let noStud = document.createElement('noStud');
            noStud.setAttribute('class', 'p');
            noStud.innerHTML = 'Студентов больше нет!';
            list.appendChild(noStud);
        }

        function avgEstimatePerCourse(){
        let courseArr = [];
        let courseUnique = [];
        let resultArr = {};
      
        for(let i in students){
            courseArr.push(students[i].course);
        }
        for(let i in courseArr){
            if(courseUnique.indexOf(courseArr[i]) == -1){
                courseUnique.push(courseArr[i]);
            }
        }
        for(let i in courseUnique){
            resultArr[courseUnique[i]] = 0;
        }
        for(let i  = 1; i <= Object.keys(resultArr).length; i++){
            let estimateAverage = [];
            for(let j in students){
                if(students[j].course == i){
                    estimateAverage.push(students[j].estimate);
                    resultArr[i] += students[j].estimate;
                }
            }
            resultArr[i] = Math.round(resultArr[i]/estimateAverage.length);
        }
        console.log(resultArr);

        for(let i in resultArr){
        var averageOut = document.createElement('estimate');
            if(resultArr[i] <= 3){
                averageOut.setAttribute('class', 'lessL');
                averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
            }else if(resultArr[i] >= 4 && resultArr[i] <= 5){
                averageOut.setAttribute('class', 'midleM');
                averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
            }else if(resultArr[i] >= 5){
                averageOut.setAttribute('class', 'moreM');
                averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
            }else if((resultArr[i] === NaN) === false){
                
                averageOut.innerHTML = '';
                
            }
           
            list.appendChild(averageOut);
        }
    }avgEstimatePerCourse();

    function unActive(){
        let unActiveStud = [];
        var unActiveStudents = document.createElement('unActive');
        unActiveStudents.setAttribute('class', 'p');

        for(let i in students){
            if(students[i].active == false){
                unActiveStud.push(students[i]);
            }
        }
        unActiveStudents.innerHTML = 'Всего неактивных студентов: ' + Object.keys(unActiveStud).length;
        list.appendChild(unActiveStudents);
    }unActive();
}createStud();

function createNewStudents(){
    let submit = document.querySelector('.submit');
 
    submit.addEventListener('click', event => {
        let form = document.querySelector('form');
        let chbox = document.getElementById('chbox');
        let isActive;
        if(chbox.checked){
            isActive = true;
        }else{
            isActive = false;
        }
        for(let i = 0; i < 3; i++){
        let newStudent = {name: form[i].value,
                         estimate: parseInt(form[i+1].value),
                         course: parseInt(form[i+2].value),
                         active: isActive 
                     }
                     students.push(newStudent);
                     console.log(newStudent);
                     console.log(students);
                     console.log(isActive)
                     break;
                 }
                 document.querySelector('.list').innerHTML = '';
                 createStud();
    })
 }createNewStudents();


}
