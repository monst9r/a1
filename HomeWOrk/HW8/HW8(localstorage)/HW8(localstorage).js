'use strict'

window.onload = function(){ 
    var oldStudents = [
    {name: 'Ivan', 
        estimate: 4, 
        course: 1, 
        active: true,
        e_mail: 'ivan@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Petro', 
        estimate: 3, 
        course: 1, 
        active: true,
        e_mail: 'petro@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Sergiy', 
        estimate: 2, 
        course: 4, 
        active: false,
        e_mail: 'sergiy@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Vasilii',
        estimate: 5, 
        course: 2, 
        active: true,
        e_mail: 'vasilii@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Vitalii',
        estimate: 5, 
        course: 2, 
        active: true,
        e_mail: 'vitalii@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Jurii',
        estimate: 3, 
        course: 3, 
        active: true,
        e_mail: 'jurii@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Artem',
        estimate: 2, 
        course: 4, 
        active: true,
        e_mail: 'artem@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Evgenii',
        estimate: 5, 
        course: 1, 
        active: true,
        e_mail: 'evgenii@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Volodya',
        estimate: 4, 
        course: 3, 
        active: false,
        e_mail: 'volodya@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Sasha',
        estimate: 4, 
        course: 4, 
        active: true,
        e_mail: 'sasha@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Silvestr',
        estimate: 3, 
        course: 2, 
        active: true,
        e_mail: 'silvestr@gmail.com',
        date: '12.12.2020 20:45'
    },
    {name: 'Mikle',
        estimate: 2, 
        course: 1, 
        active: false,
        e_mail: 'mikle@gmail.com',
        date: '12.12.2020 20:45'
    }
];
//localStorage.clear();
//localStorage.setItem('Students', JSON.stringify(oldStudents));
var students = JSON.parse(localStorage.getItem('Students'));




function createStud(){
    let list = document.querySelector('.list');
                let table = document.createElement('table');
                list.appendChild(table);
                let tr = document.createElement('tr');
                table.appendChild(tr);
                let th1 = document.createElement('th');
                let th2 = document.createElement('th');
                let th3 = document.createElement('th');
                let th4 = document.createElement('th');
                let th5 = document.createElement('th');
                let th6 = document.createElement('th');
                th1.innerHTML = 'First name';
                th2.innerHTML = 'Estimate';
                th3.innerHTML = 'Course';
                th4.innerHTML = 'Active';
                th5.innerHTML = 'E-mail';
                th6.innerHTML = 'date';
                tr.appendChild(th1);
                tr.appendChild(th2);
                tr.appendChild(th3);
                tr.appendChild(th5);
                tr.appendChild(th6);
                tr.appendChild(th4);
                let submit = document.querySelector('.submit');
             
                submit.addEventListener('click', event => {
                    let form = document.querySelector('form');
                    let chbox = document.getElementById('chbox');
                    let isActive;
                    let liveDate = new Date();
                    let newName = null;
                    let newEstimate = null;
                    let newCourse = null;
                    let newEmail = null;
                    let index = 0;
                    let newStudent;
                    if(chbox.checked){
                        isActive = true;
                    }else{
                        isActive = false;
                    }
                    if(/[0-9]/.test(form[index].value) === false && /[<>()\[\]\\.\-\_,;:\s@"]/.test(form[index].value) === false && form[index].value.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                        newName = form[index].value;
                        index++;                  
                    }else{
                        return alert('Имя может состоят только из букв, не более 15-ти!');
                    }
                    if(/[1-5]/.test(form[index].value) === true && form[index].value.match(/[0-9]/g).length === 1){
                        newEstimate = form[index].value;
                        index++;
                    }else{
                        return alert('Оценка может быть от 1 до 5!');
                    }
                    if(/[1-5]/.test(form[index].value) === true && form[index].value.match(/[0-9]/g).length === 1){
                        newCourse = form[index].value;
                        index++;
                    }else{
                        return alert('Курс может быть от 1 до 5!');
                    }
                    if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(form[index].value) === true){
                        newEmail = form[index].value;
                    }else{
                        return alert('Вы ввели некоректный E-mail адрес!');
                    }
                    newStudent = {name: newName,
                        estimate: parseInt(newEstimate),
                        course: parseInt(newCourse),
                        active: isActive, 
                        e_mail: newEmail,
                        date: liveDate.getDate() + '.' + (liveDate.getUTCMonth() + 1) + '.' + liveDate.getFullYear() + ' ' + liveDate.getHours() + ':' + liveDate.getMinutes()
                    }
                    students.push(newStudent);
                    localStorage.setItem('Students', JSON.stringify(students));
                    document.querySelector('.list').innerHTML = '';
                    createStud();
                });
    
        for(let i in students){
            var trTr = document.createElement('tr')
            table.appendChild(trTr);
            var createStudents = document.createElement('td');
            createStudents.id = i +'n';
            var changeActive = document.createElement('input');
            var deleteStud = document.createElement('img');
            var estimateT = document.createElement('td');
            estimateT.id = i + 'e';
            var courseT = document.createElement('td');
            courseT.id = i + 'c';
            var e_mailT = document.createElement('td');
            var setDate = document.createElement('td');
            setDate.innerHTML = students[i].date; 
            e_mailT.innerHTML = students[i].e_mail;
            estimateT.innerHTML = students[i].estimate;
            courseT.innerHTML = students[i].course;
            deleteStud.src = 'krest.png';
            deleteStud.width = '12';
            deleteStud.height = '12';
            changeActive.type = 'submit';
            if(students[i].active == true){
                changeActive.value = 'active';
            }else if(students[i].active == false){
                changeActive.value = 'un active';
            }
            changeActive.id = 'changeActiveStudents';
             
                if(students[i].estimate <= 3){
                    trTr.setAttribute('class', 'less');
                }else if(students[i].estimate >= 4 && students[i].estimate <5){
                    trTr.setAttribute('class', 'midle');
                }else if(students[i].estimate >= 5){
                    trTr.setAttribute('class', 'more');
                }
            createStudents.innerHTML = students[i].name;
            
            (function (i){
                createStudents.addEventListener('click', function(event){
                    let idName = i +'n';
                    let tdFirstName = document.getElementById(idName);
                    let inputName = document.createElement('input');
                    inputName.type = 'text';
                    inputName.className = 'input';
                    inputName.value = students[i].name;
                    tdFirstName.innerHTML = '';
                    tdFirstName.appendChild(inputName);
                    inputName.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            if(/[0-9]/.test(event.target.value) === false && /[<>()\[\]\\.\-\_,;:\s@"]/.test(event.target.value) === false && event.target.value.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                                students[i].name = event.target.value;
                                        localStorage.setItem('Students', JSON.stringify(students));
                                        list.innerHTML = '';
                                        createStud();                
                            }else{
                                list.innerHTML = '';
                                createStud()
                                return alert('Имя может состоят только из букв, не более 15-ти!');
                            }
                        }
                    });
                    inputName.focus();
                });
            })(i);

            (function (i){
                estimateT.addEventListener('click', function(event){
                    let idEstimate = i +'e';
                    let tdEstimate = document.getElementById(idEstimate);
                    let inputEstimate = document.createElement('input');
                    inputEstimate.type = 'text';
                    inputEstimate.className = 'input';
                    inputEstimate.value = students[i].estimate;
                    tdEstimate.innerHTML = '';
                    tdEstimate.appendChild(inputEstimate);
                    inputEstimate.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            if(/[1-5]/.test(event.target.value) === true && event.target.value.match(/[0-9]/g).length === 1){
                                students[i].estimate = parseInt(event.target.value);
                                localStorage.setItem('Students', JSON.stringify(students))
                                list.innerHTML = '';
                                createStud();
                            }else{
                                list.innerHTML = '';
                                createStud();
                                return alert('Оценка может быть от 1 до 5!');
                            }
                        }
                    });
                    inputEstimate.focus();
                });
            })(i);

            (function (i){
                courseT.addEventListener('click', function(event){
                    let idCourse = i +'c';
                    let tdCourse = document.getElementById(idCourse);
                    let inputCourse = document.createElement('input');
                    inputCourse.type = 'text';
                    inputCourse.className = 'input';
                    inputCourse.value = students[i].course;
                    tdCourse.innerHTML = '';
                    tdCourse.appendChild(inputCourse);
                    inputCourse.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            if(/[1-5]/.test(event.target.value) === true && event.target.value.match(/[0-9]/g).length === 1){
                                students[i].course = parseInt(event.target.value);
                                localStorage.setItem('Students', JSON.stringify(students))
                                list.innerHTML = '';
                                createStud();
                            }else{
                                list.innerHTML = '';
                                createStud();
                                return alert('Курс может быть от 1 до 5!');
                            }
                        }
                    });
                    inputCourse.focus();
                });
            })(i);

            (function (i, changeActive){
                changeActive.addEventListener('click', function(event){
                    if(changeActive.value == 'active'){
                        students[i].active = false;
                        localStorage.setItem('Students', JSON.stringify(students))
                        list.innerHTML = '';
                        createStud();
                    }else if(changeActive.value == 'un active'){
                        students[i].active = true;
                        localStorage.setItem('Students', JSON.stringify(students))
                        list.innerHTML = '';
                        createStud();
                    }
                })
            })(i, changeActive);
            (function (i){
                deleteStud.addEventListener('click', function(event){
                    students.splice(i, 1);
                    localStorage.setItem('Students', JSON.stringify(students))
                    list.innerHTML = '';
                    createStud();
                });
            })(i);
            
            trTr.appendChild(createStudents);
            trTr.appendChild(estimateT);
            trTr.appendChild(courseT);
            trTr.appendChild(e_mailT);
            trTr.appendChild(setDate);
            trTr.appendChild(changeActive);  
            trTr.appendChild(deleteStud);
            
            
            
           
            
        }

        if(document.querySelector('.less') == null && document.querySelector('.midle') == null && document.querySelector('.more') == null){
            let noStud = document.createElement('noStud');
            noStud.setAttribute('class', 'p');
            noStud.innerHTML = 'Студентов больше нет!';
            list.appendChild(noStud);
        }

        function avgEstimatePerCourse(){
        let courseArr = [];
        let courseUnique = [];
        let resultArr = {};
      
        for(let i in students){
            courseArr.push(students[i].course);
        }
        for(let i in courseArr){
            if(courseUnique.indexOf(courseArr[i]) == -1){
                courseUnique.push(courseArr[i]);
            }
        }
        for(let i in courseUnique){
            resultArr[courseUnique[i]] = 0;
        }
        for(let i  = 1; i <= Object.keys(resultArr).length; i++){
            let estimateAverage = [];
            for(let j in students){
                if(students[j].course == i){
                    estimateAverage.push(students[j].estimate);
                    resultArr[i] += students[j].estimate;
                }
            }
            resultArr[i] = Math.round(resultArr[i]/estimateAverage.length);
        }
        console.log(resultArr);

        for(let i in resultArr){
        var averageOut = document.createElement('estimate');
            if(resultArr[i] <= 3){
                averageOut.setAttribute('class', 'lessL');
                averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
            }else if(resultArr[i] >= 4 && resultArr[i] < 5){
                averageOut.setAttribute('class', 'midleM');
                averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
            }else if(resultArr[i] >= 5){
                averageOut.setAttribute('class', 'moreM');
                averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
            }else if((resultArr[i] === NaN) === false){
                
                averageOut.innerHTML = '';
                
            }
           
            list.appendChild(averageOut);
        }
    }avgEstimatePerCourse();

    function unActive(){
        let unActiveStud = [];
        var unActiveStudents = document.createElement('unActive');
        unActiveStudents.setAttribute('class', 'p');

        for(let i in students){
            if(students[i].active == false){
                unActiveStud.push(students[i]);
            }
        }
        unActiveStudents.innerHTML = 'Всего неактивных студентов: ' + Object.keys(unActiveStud).length;
        list.appendChild(unActiveStudents);
    }unActive();

    function newStudentsPerLastHourse (newStudent){
        let data = new Date();
        let currentTime = data.getDate() + '.' + (data.getUTCMonth() + 1) + '.' + data.getFullYear() + ' ' + data.getHours();
        var newStudentsPerHourse = document.createElement('new_students');
        newStudentsPerHourse.setAttribute('class', 'p');
        for(let i in students){
            if(students[i].date.indexOf(currentTime) !== -1){
                newStudent++;
            }
        }
        newStudentsPerHourse.innerHTML = 'Начиная с ' + data.getHours() + ':00 было добавлено: ' + newStudent + ' новых студентов';
        list.appendChild(newStudentsPerHourse);
    }newStudentsPerLastHourse(0);
}createStud();
}
