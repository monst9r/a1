/*Использовать функцию из предыдущего задания чтобы получить
массив из нужного количества значений. Найти процентное 
соотношение отрицательных, положительных и нулевых элементов 
массива.
*/
var plus = [];
var minus = [];
var zero = [];
var mas = [];
var N = -3;
var M = 12;
var b = 7;//количество элементов, которые нужно сгенерировать


function res(a){
    if(a < b){
        let i;
        i = Math.floor((Math.random()*Math.random()*(M - N +1) + N));
        mas.push(i);
        
    }else if(a > b){
        console.log(mas);
        return;
    }
    res(++a);
}
res(0);
for(var i = 0; i < mas.length; i++){
    if(mas[i] < 0){
        minus.push(mas[i]);       
    }else if(mas[i] > 0){
        plus.push(mas[i]);
    }else if(mas[i] == 0){
        zero.push(mas[i]);
    }
}
var procent = b/100
console.log('Положительных чисел ' + (plus.length/procent) + '%');
console.log('Отрицательных чисел ' + (minus.length/procent) + '%');
console.log('Нолевых элементов ' + (zero.length/procent) + '%');
