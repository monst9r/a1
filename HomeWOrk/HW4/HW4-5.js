/*
Написать рекурсивную функцию которая выводит абсолютно все 
элементы ассоциативного массива (объекта) - любого уровня 
вложенности
 */

 var mas = [{a: [5,6,8], b: 'Hello', c: {c1: 5, c2: 3}, d: 777}];



function HW(i){
   if(i < mas.length){
      console.log(mas[i]);
   }else if(i > mas.length){
      return;
   }
   HW(++i)
}
HW(0)
