'use strict'
/*
Задан массив объектов студентов вида [{name: “Ivan”, estimate: 4, course: 1, active: true},
    {name: “Ivan”, estimate: 3, course: 1, active: true},{name: “Ivan”, estimate: 2, course: 4, active: false},
    {name: “Ivan”, estimate: 5, course: 2, active: true}] - заполнить его более большим количеством студентов. 
    Написать функцию которая возвращает: среднюю оценку студентов в разрезе каждого курса: {1: 3.2, 2: 3.5, 3: 4.5, 4: 3, 5: 4.5} 
    с учетом только тех студентов которые активны. Посчитать количество неактивных студентов в разрезе каждого курса и общее 
    количество неактивных студентов.
*/
var mas = [
    {name: 'Ivan', 
    estimate: 4, 
    course: 1, 
    active: true
},
    {name: 'Petro', 
        estimate: 3, 
        course: 1, 
        active: true
    },
    {name: 'Sergiy', 
        estimate: 2, 
        course: 4, 
        active: false
    },
    {name: 'Vasilii',
        estimate: 5, 
        course: 2, 
        active: true
    },
    {name: 'Vitalii',
        estimate: 5, 
        course: 2, 
        active: true
    },
    {name: 'Jurii',
        estimate: 3, 
        course: 3, 
        active: true
    },
    {name: 'Artem',
        estimate: 2, 
        course: 4, 
        active: true
    },
    {name: 'Evgenii',
        estimate: 5, 
        course: 1, 
        active: true
    },
    {name: 'Volodya',
        estimate: 4, 
        course: 3, 
        active: false
    },
    {name: 'Sasha',
        estimate: 4, 
        course: 4, 
        active: true
    },
    {name: 'Silvestr',
        estimate: 3, 
        course: 2, 
        active: true
    },
    {name: 'Mikle',
        estimate: 2, 
        course: 1, 
        active: false
    }
]

function averageEstimatePerCourse(){
    let masArr = [];
    let courseArr = [];
    let estimateArr = [];
    let est;
    let resultArr = {};

    for(let i = 0; i < mas.length; i++){
       if(mas[i].active == true){
           masArr.push(mas[i]); 
        }   
    }
    for(let i in masArr){
    courseArr.push(masArr[i].course)
    }     
     let courseUnique = [];
    for(let i in courseArr){
        if(courseUnique.indexOf(courseArr[i]) == -1){
            courseUnique.push(courseArr[i])
        }
    }
    for(let i in courseUnique){
        resultArr[courseUnique[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(resultArr).length; i++){
    let estimateAverage = [];
    for(let j in masArr){
        if(masArr[j].course == i){
            estimateAverage.push(masArr[j].estimate)
            resultArr[i] += masArr[j].estimate
        }
    }
    resultArr[i] = resultArr[i]/estimateAverage.length
}
   console.log('Средняя оценка по курсам:')
   console.log(resultArr)
}
averageEstimatePerCourse();


function unActiveStudents(){
    let unActiveStud = [];
    
    let courseStudents = {};
    for(let i in mas){
        if(mas[i].active == false){
            unActiveStud.push(mas[i]);
        }
    }
    console.log('Всего неактивных студентов: ' + Object.keys(unActiveStud).length)
   
    let courseUnique = [];
    for(let i in mas){
        if(courseUnique.indexOf(mas[i].course) == -1){
            courseUnique.push(mas[i].course)
        }
    }
    for(let i in courseUnique){
        courseStudents[courseUnique[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(courseStudents).length; i++){
        let unActiveStudPerCourse = [];
    for(let j in mas){
        if(mas[j].course == i && mas[j].active == false){
            unActiveStudPerCourse.push(true)     
        }
    }
    courseStudents[i] = unActiveStudPerCourse.length
}
console.log('Неактивных студентов по курсам:')
console.log(courseStudents)
}
unActiveStudents();
