'use strict'
window.onload = function (){
let questions = [
    {
        title: "Что такое javascript?",
        answers: [
            {
                title: "Это скриптовый язык",
                is_correctly: true,
            },{
                title: "Это не язык язык",
                is_correctly: false,
            },{
                title: "Это  что-то еще",
                is_correctly: false,
            }
        ],
    },{
        title: "Какие утверждения верны",
        answers: [
            {
                title: "Значение переменной не изменно",
                is_correctly: false,
            },{
                title: "Значение переменной может изменяться",
                is_correctly: true,
            },{
                title: "Строки не изменны",
                is_correctly: true,
            },{
                title: "Строки изменяыемые",
                is_correctly: false,
            },{
                title: "Замыкание это возможность вызова функции",
                is_correctly: false,
            },{
                title: "Замыкание это возможность получения доступа к переменным из родительского лексического окружения",
                is_correctly: true,
            }
        ],
    },{
        title: "Что такое javascript?",
        answers: [
            {
                title: "Это скриптовый язык",
                is_correctly: true,
            },{
                title: "Это не язык язык",
                is_correctly: false,
            },{
                title: "Это  что-то еще",
                is_correctly: false,
            }
        ],
    },{
        title: "Какие утверждения верны",
        answers: [
            {
                title: "Значение переменной не изменно",
                is_correctly: false,
            },{
                title: "Значение переменной может изменяться",
                is_correctly: true,
            },{
                title: "Строки не изменны",
                is_correctly: true,
            },{
                title: "Строки изменяыемые",
                is_correctly: false,
            },{
                title: "Замыкание это возможность вызова функции",
                is_correctly: false,
            },{
                title: "Замыкание это возможность получения доступа к переменным из родительского лексического окружения",
                is_correctly: true,
            }
        ],
    },{
        title: "Что такое javascript?",
        answers: [
            {
                title: "Это скриптовый язык",
                is_correctly: true,
            },{
                title: "Это не язык язык",
                is_correctly: false,
            },{
                title: "Это  что-то еще",
                is_correctly: false,
            }
        ],
    },{
        title: "Какие утверждения верны",
        answers: [
            {
                title: "Значение переменной не изменно",
                is_correctly: false,
            },{
                title: "Значение переменной может изменяться",
                is_correctly: true,
            },{
                title: "Строки не изменны",
                is_correctly: true,
            },{
                title: "Строки изменяыемые",
                is_correctly: false,
            },{
                title: "Замыкание это возможность вызова функции",
                is_correctly: false,
            },{
                title: "Замыкание это возможность получения доступа к переменным из родительского лексического окружения",
                is_correctly: true,
            }
        ],
    }
];

function createQuestions(indexOfQustion, points){
    if(indexOfQustion < questions.length){
    
    let body = document.querySelector('body');
    var form = document.createElement('form');
    
    body.appendChild(form);
    
    let button = document.createElement('button');
    button.type = 'button';
    button.innerHTML = 'ответить';
    var label = document.createElement('label');
    label.innerHTML = questions[indexOfQustion].title;
    form.appendChild(label);
    let div = document.createElement('div');
    div.setAttribute('class', '.p');
    div.innerHTML = '';
    label.appendChild(div);

    for(let j in questions[indexOfQustion].answers){
        let span = document.createElement('span');
        span.innerHTML = questions[indexOfQustion].answers[j].title;
        div.appendChild(span);
        let input = document.createElement('input');
        input.type = 'checkbox';
        input.id = j;
        span.appendChild(input);
    }
    
    

    indexOfQustion++;
    label.appendChild(button);
    for(let i in questions){
       
        
    button.addEventListener('click', function(event){
        let div1 = document.querySelector('div');
        
        for(let i in questions[indexOfQustion - 1].answers){
            
            let input1 = document.getElementById(i);
            

            if(input1.checked === true){
                
                if(questions[indexOfQustion - 1].answers[i].is_correctly === true){
                    
                    points++;
                   
                }
            }
            
        }
        label.innerHTML = '';
            createQuestions(indexOfQustion, points);
    });
    break;
}
    
}else{
  
    return (function(){
        let body = document.querySelector('body');
        let span = document.createElement('span');
        span.innerHTML = "Ваш результат: " + points;
        body.appendChild(span);
    })();
}


}createQuestions(0, 0);
}