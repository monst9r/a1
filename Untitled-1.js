'use strict'

window.onload = function(){
    

    function studentToAdd (){
        let submit = document.querySelector('.submit');
        let newStudent;
        
        submit.addEventListener('click', event => {
            let form = document.querySelector('form');
            let chbox = document.getElementById('chbox');
            function checName(trigerName){
                console.log(trigerName)
                if(/[0-9]/.test(trigerName) === false){
                    if(/[<>()\[\]\\.\-\_,;:\s@"]/.test(trigerName)){
                        if(trigerName.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                            return resultData;
                        }
                    }
                }
                return alert('Имя может состоять только из букв, не более 15-ти!');
            }
            function checEstimate(trigerEstimate){
                if(/[1-5]/.test(trigerEstimate) === true){
                    if(trigerEstimate.match(/[0-9]/g).length === 1){
                        return resultData;
                    }
                }
                return alert('Оценка может быть от 1 до 5!');
            }
            function checCourse(trigerName){
                if(/[1-5]/.test(trigerName) === true){
                    if(trigerName.match(/[0-9]/g).length === 1){
                        return resultData;
                    }
                }
                return alert('Курс может быть от 1 до 5!')
            }
            let index = null;
            var checNewStudentArr = [checName(index), checEstimate(index), checCourse(index)];
            let isActive;
            let firstName;
            let estimateNew;
            let courseNew;
            let newStudData = [firstName, estimateNew, courseNew];
            if(chbox.checked){
                isActive = true;
            }else{
                isActive = false;
            }
            console.log(form[0])
            for(let i = 0 ; i < checNewStudentArr.length; i++){
                let index = i;
                let resultData = null;
                checNewStudentArr[form[i].value];
                if(resultData != null){
                newStudData[i] = resultData;
                }else{
                    getInfoFromServer();
                    break;
                }
            }
            newStudent = {first_name: firstName,
                estimate: parseInt(estimateNew),
                course: parseInt(courseNew),
                active: isActive
            }
                    document.querySelector('.list').innerHTML = '';
                    addInfoToServer(newStudent);
        });
    }studentToAdd();

    function removeFromServer(deleteStudent){
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', 'https://evgeniychvertkov.com/api/student/' + deleteStudent.id + '/', true);
        xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
        xhr.setRequestHeader("X-Authorization-Token", "8ee7c2a8-3f0f-11eb-a483-f1c3feb07438");
        xhr.send();
        xhr.onreadystatechange = function(){
        if(xhr.readyState != 4) return;
            if(xhr.status == 200){
        }
    }
        getInfoFromServer();
    }

    function updateFromServer(updateStudent){
        var xhr = new XMLHttpRequest();
        xhr.open('PUT', 'https://evgeniychvertkov.com/api/student/', true);
        xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
        xhr.setRequestHeader("X-Authorization-Token", "8ee7c2a8-3f0f-11eb-a483-f1c3feb07438");
        xhr.send(JSON.stringify(updateStudent));
        xhr.onreadystatechange = function(){
        if(xhr.readyState != 4) return;
            if(xhr.status == 200){
            getInfoFromServer();
            }
        }
    }

    function addInfoToServer(newStudent){
        if(newStudent !== undefined){
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'https://evgeniychvertkov.com/api/student/', true);
        xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
        xhr.setRequestHeader("X-Authorization-Token", "8ee7c2a8-3f0f-11eb-a483-f1c3feb07438");
        xhr.send(JSON.stringify(newStudent));
        xhr.onreadystatechange = function(){
        if(xhr.readyState != 4) return;
            if(xhr.status == 200){
                let response = JSON.parse(xhr.responseText);
                getInfoFromServer();
            }
        }
    }
    }

        function getInfoFromServer (){
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'https://evgeniychvertkov.com/api/student/', true);
            xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
            xhr.setRequestHeader("X-Authorization-Token", "8ee7c2a8-3f0f-11eb-a483-f1c3feb07438");
            xhr.send();
            xhr.onreadystatechange = function(){
            if(xhr.readyState != 4) return;
                if(xhr.status == 200){
                    if(document.querySelector('.mail') != null){
                    let form = document.querySelector('form');  
                    form.removeChild(document.querySelector('.mail'));
                    }
                    let response = JSON.parse(xhr.responseText);
                    let students = response.students;
                    let list = document.querySelector('.list');
                    list.innerHTML = '';
                    let table = document.createElement('table');
                    list.appendChild(table);
                    let tr = document.createElement('tr');
                    table.appendChild(tr);
                    let th1 = document.createElement('th');
                    let th2 = document.createElement('th');
                    let th3 = document.createElement('th');
                    let th4 = document.createElement('th');
                    th1.innerHTML = 'First name';
                    th2.innerHTML = 'Estimate';
                    th3.innerHTML = 'Course';
                    th4.innerHTML = 'Active';
                    tr.appendChild(th1);
                    tr.appendChild(th2);
                    tr.appendChild(th3);
                    tr.appendChild(th4);
    
                    for(let i in students){
                        var trTr = document.createElement('tr')
                        table.appendChild(trTr);
                        var createStudents = document.createElement('td');
                        createStudents.id = i +'n';
                        var changeActive = document.createElement('input');
                        var deleteStud = document.createElement('img');
                        var estimateT = document.createElement('td');
                        estimateT.id = i + 'e';
                        var courseT = document.createElement('td');
                        courseT.id = i + 'c';
                        estimateT.innerHTML = students[i].estimate;
                        courseT.innerHTML = students[i].course;
                        deleteStud.src = 'krest.png';
                        deleteStud.width = '12';
                        deleteStud.height = '12';
                        changeActive.type = 'submit';
                        if(students[i].is_active == true){
                            changeActive.value = 'active';
                        }else if(students[i].is_active == false){
                            changeActive.value = 'un active';
                        }
                        changeActive.id = 'changeActiveStudents';
                        
                            if(students[i].estimate <= 3){
                                trTr.setAttribute('class', 'less');
                            }else if(students[i].estimate >= 4 && students[i].estimate <5){
                                trTr.setAttribute('class', 'midle');
                            }else if(students[i].estimate >= 5){
                                trTr.setAttribute('class', 'more');
                            }
                        createStudents.innerHTML = students[i].first_name;
                        
                        (function (i){
                            createStudents.addEventListener('click', function(event){
                                let idName = i +'n';
                                let tdFirstName = document.getElementById(idName);
                                let inputName = document.createElement('input');
                                inputName.type = 'text';
                                inputName.className = 'input';
                                inputName.value = students[i].first_name;
                                tdFirstName.innerHTML = '';
                                tdFirstName.appendChild(inputName);
                                inputName.addEventListener('keydown', function(event){
                                    if(event.keyCode === 13){
                                        if(/[0-9]/.test(event.target.value) === false){
                                            if(/[<>()\[\]\\.\-\_,;:\s@"]/.test(event.target.value) === false){
                                                if(event.target.value.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                                                    let updateName = {
                                                        id: students[i].id,
                                                        first_name: event.target.value,
                                                        estimate: students[i].estimate,
                                                        course: students[i].course,
                                                        is_active: students[i].is_active
                                                    }
                                                    updateFromServer(updateName);
                                                }else{
                                                    getInfoFromServer();
                                                    return alert('Имя может состоять только из букв!');
                                                }
                                            }else{
                                                getInfoFromServer();
                                                return alert('Имя может состоять только из букв!');
                                            }
                                        }else{
                                            getInfoFromServer();
                                            return alert('Имя может состоять только из букв!');
                                        }
                                    }
                                });
                                inputName.focus();
                            });
                        })(i);

                        (function (i){
                            estimateT.addEventListener('click', function(event){
                                let idEstimate = i +'e';
                                let tdEstimate = document.getElementById(idEstimate);
                                let inputEstimate = document.createElement('input');
                                inputEstimate.type = 'text';
                                inputEstimate.className = 'input';
                                inputEstimate.value = students[i].estimate;
                                tdEstimate.innerHTML = '';
                                tdEstimate.appendChild(inputEstimate);
                                inputEstimate.addEventListener('keydown', function(event){
                                    if(event.keyCode === 13){
                                        if(/[1-5]/.test(event.target.value) === true){
                                            if(event.target.value.match(/[0-9]/g).length === 1){
                                                let updateName = {
                                                    id: students[i].id,
                                                    first_name: students[i].first_name,
                                                    estimate: event.target.value,
                                                    course: students[i].course,
                                                    is_active: students[i].is_active
                                                }
                                                updateFromServer(updateName);
                                            }else{
                                                getInfoFromServer();
                                                return alert('Оценка может быть от 1 до 5!');
                                            }
                                        }else{
                                            getInfoFromServer();
                                            return alert('Оценка может быть от 1 до 5!');
                                        }
                                    }
                                });
                                inputEstimate.focus();
                            });
                        })(i);

                        (function (i){
                            courseT.addEventListener('click', function(event){
                                let idCourse = i +'c';
                                let tdCourse = document.getElementById(idCourse);
                                let inputCourse = document.createElement('input');
                                inputCourse.type = 'text';
                                inputCourse.className = 'input';
                                inputCourse.value = students[i].course;
                                tdCourse.innerHTML = '';
                                tdCourse.appendChild(inputCourse);
                                inputCourse.addEventListener('keydown', function(event){
                                    if(event.keyCode === 13){
                                        if(/[1-5]/.test(event.target.value) === true){
                                            if(event.target.value.match(/[0-9]/g).length === 1){
                                                let updateName = {
                                                    id: students[i].id,
                                                    first_name: students[i].first_name,
                                                    estimate: students[i].estimate,
                                                    course: event.target.value,
                                                    is_active: students[i].is_active
                                                }
                                                updateFromServer(updateName);
                                            }else{
                                                getInfoFromServer();
                                                return alert('Курс может быть от 1 до 5!');
                                            }
                                        }else{
                                            getInfoFromServer();
                                            return alert('Курс может быть от 1 до 5!');
                                        }
                                    }
                                });
                                inputCourse.focus();
                            });
                        })(i);

                        (function (i, changeActive){
                        changeActive.addEventListener('click', function(event){
                                if(changeActive.value == 'active'){
                                    let readyStudentToUpdate = {
                                        id: students[i].id,
                                        first_name: students[i].first_name,
                                        estimate: students[i].estimate,
                                        course: students[i].course,
                                        active: false
                                    }
                                    updateFromServer(readyStudentToUpdate);
                                }else if(changeActive.value == 'un active'){
                                    let readyStudentToUpdate = {
                                        id: students[i].id,
                                        first_name: students[i].first_name,
                                        estimate: students[i].estimate,
                                        course: students[i].course,
                                        active: true
                                    }
                                    updateFromServer(readyStudentToUpdate);
                                }  
                            })
                        })(i, changeActive);

                        (function (i){
                            deleteStud.addEventListener('click', function(event){ 
                                list.innerHTML = '';
                                removeFromServer(students[i]);
                            });
                        })(i);
                        
                        trTr.appendChild(createStudents);
                        trTr.appendChild(estimateT);
                        trTr.appendChild(courseT);
                        trTr.appendChild(changeActive);  
                        trTr.appendChild(deleteStud);
                    }
            if(document.querySelector('.less') == null && document.querySelector('.midle') == null && document.querySelector('.more') == null){
                let noStud = document.createElement('noStud');
                noStud.setAttribute('class', 'p');
                noStud.innerHTML = 'Студентов больше нет!';
                list.appendChild(noStud);
            }
    
            function avgEstimatePerCourse(){
            let courseArr = [];
            let courseUnique = [];
            let resultArr = {};
          
            for(let i in students){
                courseArr.push(students[i].course);
            }
            for(let i in courseArr){
                if(courseUnique.indexOf(courseArr[i]) == -1){
                    courseUnique.push(courseArr[i]);
                }
            }
            for(let i in courseUnique){
                resultArr[courseUnique[i]] = 0;
            }
            for(let i  = 1; i <= Object.keys(resultArr).length; i++){
                let estimateAverage = [];
                for(let j in students){
                    if(students[j].course == i){
                        estimateAverage.push(students[j].estimate);
                        resultArr[i] += students[j].estimate;
                    }
                }
                resultArr[i] = Math.round(resultArr[i]/estimateAverage.length);
            }
            for(let i in resultArr){
            var averageOut = document.createElement('estimate');
                if(resultArr[i] <= 3){
                    averageOut.setAttribute('class', 'lessL');
                    averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                }else if(resultArr[i] >= 4 && resultArr[i] < 5){
                    averageOut.setAttribute('class', 'midleM');
                    averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                }else if(resultArr[i] >= 5){
                    averageOut.setAttribute('class', 'moreM');
                    averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                }else if((resultArr[i] === NaN) === false){
                    
                    averageOut.innerHTML = '';
                    
                }
               
                list.appendChild(averageOut);
            }
        }avgEstimatePerCourse();
    
        function unActive(){    
            let unActiveStud = [];
            var unActiveStudents = document.createElement('unActive');
            unActiveStudents.setAttribute('class', 'p');
    
            for(let i in students){ 
                if(students[i].is_active == false){
                    unActiveStud.push(students[i]);
                }
            }
            unActiveStudents.innerHTML = 'Всего неактивных студентов: ' + Object.keys(unActiveStud).length;
            list.appendChild(unActiveStudents);
        }unActive();
        }else{
            var students = JSON.parse(localStorage.getItem('Students'));

            function createStud(){
                let list = document.querySelector('.list');
                let table = document.createElement('table');
                list.appendChild(table);
                let tr = document.createElement('tr');
                table.appendChild(tr);
                let th1 = document.createElement('th');
                let th2 = document.createElement('th');
                let th3 = document.createElement('th');
                let th4 = document.createElement('th');
                let th5 = document.createElement('th');
                let th6 = document.createElement('th');
                th1.innerHTML = 'First name';
                th2.innerHTML = 'Estimate';
                th3.innerHTML = 'Course';
                th4.innerHTML = 'Active';
                th5.innerHTML = 'E-mail';
                th6.innerHTML = 'date';
                tr.appendChild(th1);
                tr.appendChild(th2);
                tr.appendChild(th3);
                tr.appendChild(th5);
                tr.appendChild(th6);
                tr.appendChild(th4);
                
                    for(let i in students){
                        var trTr = document.createElement('tr')
                        table.appendChild(trTr);
                        var createStudents = document.createElement('td');
                        createStudents.id = i +'n';
                        var changeActive = document.createElement('input');
                        var deleteStud = document.createElement('img');
                        var estimateT = document.createElement('td');
                        estimateT.id = i + 'e';
                        var courseT = document.createElement('td');
                        courseT.id = i + 'c';
                        var e_mailT = document.createElement('td');
                        var setDate = document.createElement('td');
                        setDate.innerHTML = students[i].date; 
                        e_mailT.innerHTML = students[i].e_mail;
                        estimateT.innerHTML = students[i].estimate;
                        courseT.innerHTML = students[i].course;
                        deleteStud.src = 'krest.png';
                        deleteStud.width = '12';
                        deleteStud.height = '12';
                        changeActive.type = 'submit';
                        if(students[i].active == true){
                            changeActive.value = 'active';
                        }else if(students[i].active == false){
                            changeActive.value = 'un active';
                        }
                        changeActive.id = 'changeActiveStudents';
                         
                            if(students[i].estimate <= 3){
                                trTr.setAttribute('class', 'less');
                            }else if(students[i].estimate >= 4 && students[i].estimate <5){
                                trTr.setAttribute('class', 'midle');
                            }else if(students[i].estimate >= 5){
                                trTr.setAttribute('class', 'more');
                            }
                        createStudents.innerHTML = students[i].name;
                        
                        (function (i){
                            createStudents.addEventListener('click', function(event){
                                let idName = i +'n';
                                let tdFirstName = document.getElementById(idName);
                                let inputName = document.createElement('input');
                                inputName.type = 'text';
                                inputName.className = 'input';
                                inputName.value = students[i].name;
                                tdFirstName.innerHTML = '';
                                tdFirstName.appendChild(inputName);
                                inputName.addEventListener('keydown', function(event){
                                    if(event.keyCode === 13){
                                        if(/[0-9]/.test(event.target.value) === false){
                                            if(/[<>()\[\]\\.\-\_,;:\s@"]/.test(event.target.value) === false){
                                                if(event.target.value.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                                                    students[i].name = event.target.value;
                                                    localStorage.setItem('Students', JSON.stringify(students));
                                                    list.innerHTML = '';
                                                    createStud();
                                                }else{
                                                    list.innerHTML = '';
                                                    createStud();
                                                    return alert('Имя может состоять только из букв!');
                                                }
                                            }else{
                                                list.innerHTML = '';
                                                createStud();
                                                return alert('Имя может состоять только из букв!');
                                            }
                                        }else{
                                            list.innerHTML = '';
                                            createStud();
                                            return alert('Имя может состоять только из букв!');
                                        }
                                    }
                                });
                                inputName.focus();
                            });
                        })(i);
            
                        (function (i){
                            estimateT.addEventListener('click', function(event){
                                let idEstimate = i +'e';
                                let tdEstimate = document.getElementById(idEstimate);
                                let inputEstimate = document.createElement('input');
                                inputEstimate.type = 'text';
                                inputEstimate.className = 'input';
                                inputEstimate.value = students[i].estimate;
                                tdEstimate.innerHTML = '';
                                tdEstimate.appendChild(inputEstimate);
                                inputEstimate.addEventListener('keydown', function(event){
                                    if(event.keyCode === 13){
                                        if(/[1-5]/.test(event.target.value) === true){
                                            if(event.target.value.match(/[0-9]/g).length === 1){
                                                students[i].estimate = parseInt(event.target.value);
                                                localStorage.setItem('Students', JSON.stringify(students))
                                                list.innerHTML = '';
                                                createStud();
                                            }else{
                                                list.innerHTML = '';
                                                createStud();
                                                return alert('Оценка может быть от 1 до 5!');
                                            }
                                        }else{
                                            list.innerHTML = '';
                                            createStud();
                                            return alert('Оценка может быть от 1 до 5!');
                                        }
                                    }
                                });
                                inputEstimate.focus();
                            });
                        })(i);
            
                        (function (i){
                            courseT.addEventListener('click', function(event){
                                let idCourse = i +'c';
                                let tdCourse = document.getElementById(idCourse);
                                let inputCourse = document.createElement('input');
                                inputCourse.type = 'text';
                                inputCourse.className = 'input';
                                inputCourse.value = students[i].course;
                                tdCourse.innerHTML = '';
                                tdCourse.appendChild(inputCourse);
                                inputCourse.addEventListener('keydown', function(event){
                                    if(event.keyCode === 13){
                                        if(/[1-5]/.test(event.target.value) === true){
                                            if(event.target.value.match(/[0-9]/g).length === 1){
                                                students[i].course = parseInt(event.target.value);
                                                localStorage.setItem('Students', JSON.stringify(students))
                                                list.innerHTML = '';
                                                createStud();
                                            }else{
                                                list.innerHTML = '';
                                                createStud();
                                                return alert('Курс может быть от 1 до 5!');
                                            }
                                        }else{
                                            list.innerHTML = '';
                                            createStud();
                                            return alert('Курс может быть от 1 до 5!');
                                        }
                                    }
                                });
                                inputCourse.focus();
                            });
                        })(i);
            
                        (function (i, changeActive){
                            changeActive.addEventListener('click', function(event){
                                if(changeActive.value == 'active'){
                                    students[i].active = false;
                                    localStorage.setItem('Students', JSON.stringify(students))
                                    list.innerHTML = '';
                                    createStud();
                                }else if(changeActive.value == 'un active'){
                                    students[i].active = true;
                                    localStorage.setItem('Students', JSON.stringify(students))
                                    list.innerHTML = '';
                                    createStud();
                                }
                            })
                        })(i, changeActive);
                        (function (i){
                            deleteStud.addEventListener('click', function(event){
                                students.splice(i, 1);
                                localStorage.setItem('Students', JSON.stringify(students))
                                list.innerHTML = '';
                                createStud();
                            });
                        })(i);
                        
                        trTr.appendChild(createStudents);
                        trTr.appendChild(estimateT);
                        trTr.appendChild(courseT);
                        trTr.appendChild(e_mailT);
                        trTr.appendChild(setDate);
                        trTr.appendChild(changeActive);  
                        trTr.appendChild(deleteStud);
                        
                        
                        
                       
                        
                    }
            
                    if(document.querySelector('.less') == null && document.querySelector('.midle') == null && document.querySelector('.more') == null){
                        let noStud = document.createElement('noStud');
                        noStud.setAttribute('class', 'p');
                        noStud.innerHTML = 'Студентов больше нет!';
                        list.appendChild(noStud);
                    }
            
                    function avgEstimatePerCourse(){
                    let courseArr = [];
                    let courseUnique = [];
                    let resultArr = {};
                  
                    for(let i in students){
                        courseArr.push(students[i].course);
                    }
                    for(let i in courseArr){
                        if(courseUnique.indexOf(courseArr[i]) == -1){
                            courseUnique.push(courseArr[i]);
                        }
                    }
                    for(let i in courseUnique){
                        resultArr[courseUnique[i]] = 0;
                    }
                    for(let i  = 1; i <= Object.keys(resultArr).length; i++){
                        let estimateAverage = [];
                        for(let j in students){
                            if(students[j].course == i){
                                estimateAverage.push(students[j].estimate);
                                resultArr[i] += students[j].estimate;
                            }
                        }
                        resultArr[i] = Math.round(resultArr[i]/estimateAverage.length);
                    }
                    console.log(resultArr);
            
                    for(let i in resultArr){
                    var averageOut = document.createElement('estimate');
                        if(resultArr[i] <= 3){
                            averageOut.setAttribute('class', 'lessL');
                            averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                        }else if(resultArr[i] >= 4 && resultArr[i] < 5){
                            averageOut.setAttribute('class', 'midleM');
                            averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                        }else if(resultArr[i] >= 5){
                            averageOut.setAttribute('class', 'moreM');
                            averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                        }else if((resultArr[i] === NaN) === false){
                            
                            averageOut.innerHTML = '';
                            
                        }
                       
                        list.appendChild(averageOut);
                    }
                }avgEstimatePerCourse();
            
                function unActive(){
                    let unActiveStud = [];
                    var unActiveStudents = document.createElement('unActive');
                    unActiveStudents.setAttribute('class', 'p');
            
                    for(let i in students){
                        if(students[i].active == false){
                            unActiveStud.push(students[i]);
                        }
                    }
                    unActiveStudents.innerHTML = 'Всего неактивных студентов: ' + Object.keys(unActiveStud).length;
                    list.appendChild(unActiveStudents);
                }unActive();
            
                function newStudentsPerLastHourse (newStudent){
                    let data = new Date();
                    let currentTime = data.getDate() + '.' + (data.getUTCMonth() + 1) + '.' + data.getFullYear() + ' ' + data.getHours();
                    var newStudentsPerHourse = document.createElement('new_students');
                    newStudentsPerHourse.setAttribute('class', 'p');
                    for(let i in students){
                        if(students[i].date.indexOf(currentTime) !== -1){
                            newStudent++;
                        }
                    }
                    newStudentsPerHourse.innerHTML = 'Начиная с ' + data.getHours() + ':00 было добавлено: ' + newStudent + ' новых студентов';
                    list.appendChild(newStudentsPerHourse);
                }newStudentsPerLastHourse(0);
            
            }createStud();
            
            function createNewStudents(){
                let submit = document.querySelector('.submit');
             
                submit.addEventListener('click', event => {
                    let form = document.querySelector('form');
                    let chbox = document.getElementById('chbox');
                    let isActive;
                    let liveDate = new Date();
                    if(chbox.checked){
                        isActive = true;
                    }else{
                        isActive = false;
                    }
                    for(let i = 0; i < 4; i++){
                        if(/[0-9]/.test(form[i].value) === false){
                            if(/[<>()\[\]\\.\-\_,;:\s@"]/.test(form[i].value) === false){
                                if(form[i].value.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                                    if(/[1-5]/.test(form[i+1].value) === true){
                                        if(form[i+1].value.match(/[0-9]/g).length === 1){
                                            if(/[1-5]/.test(form[i+2].value) === true){
                                                if(form[i+2].value.match(/[0-9]/g).length === 1){
                                                    if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(form[i+3].value) === true){
            
                                                    }else{
                                                        alert('Вы ввели некоректный E-mail адрес!');
                                                        break;
                                                    }
                                                }else{
                                                    alert('Курс может быть от 1 до 5!');
                                                    break;
                                                }
                                            }else{
                                                alert('Курс может быть от 1 до 5!');
                                                break;
                                            }
                                        }else{
                                            alert('Оценка может быть от 1 до 5!');
                                            break;
                                        }
                                    }else{
                                        alert('Оценка может быть от 1 до 5!');
                                        break;
                                    }
                                }else{
                                    alert('Имя должно иметь не более 15 букв!');
                                    break;
                                }
                            }else{
                                alert('Имя может состоять только из букв!');
                                break;
                            }
                            }else{
                            alert('Имя может состоять только из букв!');
                            break;
                        }
                    let newStudent = {name: form[i].value,
                                     estimate: parseInt(form[i+1].value),
                                     course: parseInt(form[i+2].value),
                                     active: isActive, 
                                     e_mail: form[i+3].value,
                                     date: liveDate.getDate() + '.' + (liveDate.getUTCMonth() + 1) + '.' + liveDate.getFullYear() + ' ' + liveDate.getHours() + ':' + liveDate.getMinutes()
                                 }
                                 students.push(newStudent);
                                 localStorage.setItem('Students', JSON.stringify(students));
                                 break;
                             }
                             document.querySelector('.list').innerHTML = '';
                             createStud();
                })
             }createNewStudents();
        }
    }
    }
        getInfoFromServer();
}